<?php

namespace Tests\Browser;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AutomationTest extends DuskTestCase
{

    /**
     * Test access page Welcome
     *
     * @throws \Throwable
     */
    public function testVisitWelcomePage()
    {
        $this->browse(function (Browser $browser) {
            $browser
                // truy cập trang register
                ->visit('/')
                // Xác nhận nhìn thấy text Register
                ->assertSeeLink('Login')
                ->assertSeeLink('Register')
                ->assertTitle(config('setting.app_title'));
        });
    }
}
