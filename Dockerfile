FROM composer:2.0.4 as build
WORKDIR /app
COPY . /app
RUN composer update

FROM hoamaoru/k8s-laravel-real:0.0.7
EXPOSE 80
WORKDIR /app
COPY --from=build /app /app
RUN chown -R www-data:www-data /app \
    && a2enmod rewrite
